const logo = {
  title: 'Hairart<br>Fashion',
  href: '#',
};

const navBarItems = [
  {
    title: 'Home',
  },
  {
    title: 'About',
  },
  {
    title: 'Client',
  },
  {
    title: 'Store',
  },
  {
    title: 'Contact us',
  },
];

const form = [
  {
    type: 'input',
    title: 'Email',
  },
  {
    type: 'input',
    title: 'Password',
  },
  {
    type: 'button',
    title: 'Log In',
  },
  {
    type: 'link',
    href: 'https://google.com',
    title: 'Forgot password?',
  },
  {
    type: 'button',
    title: 'Sign up with Google',
  },
];

export function getLogo() {
  return { ...logo };
}

export function getNavBarItems() {
  return [...navBarItems];
}

export function getForm() {
  return [...form];
}
