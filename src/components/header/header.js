import './header.css';

export function setupHeader(parent, data) {
  // create header
  const header = createHeader(data);

  // setup header into parent HTML Element
  parent.append(header);
}

function createHeader(data) {
  const header = document.createElement('header');

  header.className = 'header';

  const logo = createLogo(data.logo);
  const nav = createNav(data.navBarItems);

  header.append(logo);
  header.append(nav);

  return header;
}

function createLogo(logo) {
    const { title, href } = logo;

    const link = document.createElement('a');

    link.className = 'header__logo';
    link.setAttribute('href', href);
    link.innerHTML = title;

    return link;
}

function createNav(items) {
  const nav = document.createElement('nav');
  const ul = document.createElement('ul');

  nav.className = 'header__nav';

  ul.className = 'header__nav-list';

  items.forEach((item) => {
    const li = document.createElement('li');

    li.className = 'header__nav-list-item';
    li.innerHTML = item.title;

    ul.append(li);
  });

  nav.append(ul);

  return nav;
}
