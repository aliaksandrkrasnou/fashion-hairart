export function setupMain(parent) {
  const main = document.createElement('main');

  main.className = 'main';

  main.innerHTML = `
        <div>Image</div>

        <div id="sign-in-form__container"></div>
    `;

  parent.append(main);
}
