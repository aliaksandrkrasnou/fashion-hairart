import './form.css';

export function setupForm(parent, data) {
  const form = createForm(data);

  parent.append(form);

  return form;
}

function createForm(data) {
  const { form } = data;

  const formElement = document.createElement('form');

  formElement.className = 'sign-in-form__form';

  form.forEach((element) => {
    const { type, title } = element;

    if (type === 'button') {
      const button = document.createElement('button');

      button.innerHTML = title;

      formElement.append(button);
    }

    if (type === 'input') {
      const input = document.createElement('input');

      input.setAttribute('placeholder', title);

      formElement.append(input);
    }

    if (type === 'link') {
      const link = document.createElement('a');

      const { href } = element;
      link.setAttribute('href', href);
      link.innerHTML = title;

      formElement.append(link);
    }
  });

  return formElement;
}
