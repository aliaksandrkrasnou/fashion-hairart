import './style.css';
import { setupHeader } from './components/header/header';
import { getForm, getLogo, getNavBarItems } from './api/api';
import { setupForm } from './components/sign-in-form/form';
import { setupMain } from './components/main/main';

setupHeader(document.getElementById('app'), {
  logo: getLogo(),
  navBarItems: getNavBarItems(),
});

setupMain(document.getElementById('app'));

setupForm(document.getElementById('sign-in-form__container'), {
  form: getForm(),
});
